var searchData=
[
  ['n_5fdof_219',['n_dof',['../namespacevariables.html#a668867c2ac7e34a0663f1915b2165387',1,'variables']]],
  ['n_5finit_5fbo_220',['n_init_bo',['../namespacevariables.html#a54d9fd995f19db5f6c7d693866d83845',1,'variables']]],
  ['new_5fpotential_221',['new_potential',['../namespacevariables.html#a3834e5921df11cbddfa2c96ccda4bd08',1,'variables']]],
  ['npairs_222',['npairs',['../namespacevariables.html#a836f9ab38c17f0326b50b960414bbe66',1,'variables']]],
  ['nrg_5fcheck_223',['nrg_check',['../namespacevariables.html#a773129925702c06b767ee5948e6d8369',1,'variables']]],
  ['nrg_5fgap_224',['nrg_gap',['../namespacevariables.html#a0ffb326d7311be33ba677ed84d5b6ba4',1,'variables']]],
  ['nstates_225',['nstates',['../namespacevariables.html#af011425a070282cdf4f7869cc91b2e7c',1,'variables']]],
  ['nsteps_226',['nsteps',['../namespacevariables.html#a55efc2aa607a220f981be3b6fa94a1d0',1,'variables']]],
  ['ntraj_227',['ntraj',['../namespacevariables.html#a8995c056c032e0032610f890f8ccc59d',1,'variables']]]
];
